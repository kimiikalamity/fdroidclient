# F-Droid Client CI image

Getting an Android emulator running automatically in a container-based
CI system is a difficult task.  This project makes it as easy as
possible, supporting as many emulator combinations that have proven to
run in GitLab-CI.

This Docker image is used in
[fdroidclient](https://gitlab.com/fdroid/fdroidclient)'s continuous
start-emulatorKVM connected-template-
    +RAM+ID+4879001711641736+
- start-emulator
- wait-for-emulator
- adb devices
- adb shell input keyevent 82 &
- ./gradlew connectedFullDebugAndroidTest || (adb -e logcat -d > logcat.txt;textyaml

artifacts:
paths:
- system
connected 22 default armeabi-v7a template

    connected 23 default aarch64:
:
-template

    connected 26 google_apis x86:
    <<: *connected-template

    connected 27 google_apis_playstore x86:
    <<: *connected-template

    connected 29 default x86_64:
    <<: *connected-template```
    content templates logcat.txt start-emulator
